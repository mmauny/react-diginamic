import React from "react";
import { BrowserRouter, Route } from "react-router-dom";
import "./App.css";
import Detail from "./component/Detail";
import NavBar from "./component/NavBar";
import StatusContainer from "./component/StatusContainer";
import TrainingDetail from "./component/TrainingDetail";
import TrainingList from "./component/TrainingList";
import UserProvider from "./component/UserProvider";

const UserContext = React.createContext({
  user: { nom: "Matthieu Mauny" },
});

class App extends React.Component {
  render() {
    return (
      <div className="App">
        <BrowserRouter>
          <StatusContainer>
            <UserProvider>
              <UserContext.Consumer>
                {({ user }) => <NavBar user={user}></NavBar>}
              </UserContext.Consumer>
     
            <Route exact path="/" component={TrainingList}></Route>
            <Route path="/detail" component={Detail}></Route>
            <Route path="/detail/:nom" component={TrainingDetail}></Route>
            </UserProvider>
            <UserContext.Consumer>
                {({ user }) => <NavBar user={user}></NavBar>}
              </UserContext.Consumer>
          </StatusContainer>
        </BrowserRouter>
      </div>
    );
  }
}

export default App;
export { UserContext };
