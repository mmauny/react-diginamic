import React from "react";

const name = 'Diginamic'

const element = (
  <div>
     Bonjour le monde {name} je suis une classe
  </div>
  )

class WelcomeClass extends React.Component {
  render() {
    return element;
  }
}

export default WelcomeClass;