import { useContext, useEffect, useState } from 'react'
import { UserContext } from '../App'

 const FormationForms = ({onNewFormation}) => {

    const [formation, setformation] = useState('')
    const [mdp, setMdp] = useState('')
    const [mdpValidation, setValidationMdp] = useState('')
    const [formValid, setFormValid] = useState(false)

    const context = useContext(UserContext)

    useEffect(
        () => {
            setFormValid(formation.includes('@') && mdp.length >= 4 && mdp === mdpValidation)
        }, 
    [formation,mdp,mdpValidation]
    )

    const submitHandler= (event) => {
        if(formValid) {
            onNewFormation({nom :formation, id:Math.floor(Math.random()*100)})
        }
        event.preventDefault()
    }

    return (
        <form onSubmit={e => submitHandler(e)}>
            {context.user && <div>L'utilisateur est {context.user.nom}</div>}
            <label>
                Formation : 
                <input value={formation} onChange={e => setformation(e.target.value)}></input>
            </label>
            <label>
                Mot de passe : 
                <input value={mdp} type="password" onChange={e => setMdp(e.target.value)}></input>
            </label>
            <label>
                Validation mot de passe : 
                <input value={mdpValidation} type="password" onChange={e => setValidationMdp(e.target.value)}></input>
            </label>
            <button 
                onClick={e => submitHandler(e)} 
                disabled={!formValid}>
                    ajouter
            </button>
        </form>
    )

}

export default FormationForms;