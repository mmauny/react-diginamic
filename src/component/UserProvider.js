import React from "react";
import { UserContext } from "../App";

class UserProvider extends React.Component {
  constructor() {
    super();
    this.state = { nom: "Michel" };
    this.ref = React.createRef();
  }

  componentDidMount() {
    this.ref.current.focus();
  }

  render() {
    return (
      <UserContext.Provider value={{ user: this.state }}>
        <input
          ref={this.ref}
          value={this.state.nom}
          onChange={(e) =>
            this.setState({
              nom: e.target.value
            })
          }
        ></input>
        {this.props.children}
      </UserContext.Provider>
    );
  }
}

export default UserProvider;
