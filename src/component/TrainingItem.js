import React from "react";
import { Link } from "react-router-dom";

class TrainingItemClass extends React.Component {

    render() {
        return (this.shouldDisplay() && <li><Link to={`/detail/${this.props.nom}`}>{this.props.nom}</Link></li>)
    }

    shouldDisplay() {
        return this.props.nom.toLowerCase() !== "angular" 
    }

    componentDidMount() {
        console.log("componentDidMount", this.props.nom)
    }
}

export default TrainingItemClass;