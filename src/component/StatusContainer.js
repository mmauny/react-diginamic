import { useState, useEffect } from "react";

 function StatusContainerFunc({children}) {

    const [online, setOnline] = useState(true);

    useEffect(() => {
        window.addEventListener("offline", () => setOnline(false), false);
        window.addEventListener("online", () => setOnline(true), false);
    }, [])

    useEffect(() => {
        console.log("changement de statut", online)
    }, [online])

    return (
        <div>
            {children}
            {!online && <div>Statut : offline</div>}
        </div>
    )

}

export default StatusContainerFunc