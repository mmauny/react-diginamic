import { NavLink } from "react-router-dom";

const NavBar = ({ user }) => (
  <nav>
    {user && <span>{user.nom}</span>}
    <NavLink exact to="/" activeClassName="select">
      Accueil
    </NavLink>
    <NavLink to="/detail" activeClassName="select">
      Detail
    </NavLink>
  </nav>
);

export default NavBar;
