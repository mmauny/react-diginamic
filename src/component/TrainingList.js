import React, { useState } from "react";
import FormationForms from "./FormationForms";
import TrainingItemClass from "./TrainingItem";
import TrainingTitle from "./TrainingTitle";

let formationsDefault = [
{ 
    id: 13565,
    nom: 'React JS'
},
{ 
    id: 5678,
    nom: 'React Natif'
},
{ 
    id: 4589,
    nom: 'AnGuLaR'
},
{ 
    id: 85643,
    nom: 'TypeScript'
},
{ 
    id: 5793,
    nom: 'Node'
}
];

 const TrainingList = () => {

    const [formations, setFormations] = useState(formationsDefault)

    const newFormationHandler = (formation) => setFormations(previous => [...previous, formation])
    
    return (
    <div>
        <TrainingTitle></TrainingTitle>
        <FormationForms onNewFormation={(formation) => newFormationHandler(formation)}></FormationForms>
        <ul>
            {formations.map((formation) => (<TrainingItemClass key={formation.id} nom={formation.nom}/>))}
        </ul>
    </div>
    )
}

export default TrainingList;